def alphabeta(self, game, time_left, depth, alpha=float("-inf"), beta=float("inf"), maximizing_player=True, moves_lookup=None):
    """Implementation of the alphabeta algorithm

    Args:
        game (Board): A board and game state.
        time_left (function): Used to determine time left before timeout
        depth: Used to track how deep you are in the search tree
        alpha (float): Alpha value for pruning
        beta (float): Beta value for pruning
        maximizing_player (bool): True if maximizing player is active.

    Returns:
        (tuple,tuple, int): best_move_queen1,best_move_queen2, val
    """
    # Check to see if a terminal node
    if depth == 0:
        return None, None, self.utility(game, maximizing_player), False
    # Code is staggered like this for performance gains
    # Queen moves are known back in the prior function call so use None placeholders
    queen1_moves = game.get_legal_moves_of_queen1()
    if not queen1_moves:
        return None, None, self.utility(game, maximizing_player), False
    queen2_moves = game.get_legal_moves_of_queen2()
    if not queen2_moves:
        return None, None, self.utility(game, maximizing_player), False

    if maximizing_player:
        best_move_queen1 = None
        best_move_queen2 = None
        best_value = float("-inf")
        moves_to_check = None
        # TODO - try performance comparison with itertools.product() and then filter
        # duplicate tuples irrespective of their order
        # for move_queen1 in queen1_moves:
        #     for move_queen2 in queen2_moves:
        if moves_lookup[('max', depth-1)] is None:
            moves_values[('max', depth)] = []
            moves_to_check = itertools.product(queen1_moves, queen2_moves)
        else:
            moves_values[('max', depth)] = []
            moves_to_check = sorted(moves_lookup[('max', depth-1)])
        for _, prev_q1, prev_q2 in moves_to_check:
            if move_queen1 == move_queen2:
                # TODO - check if swapped locations for two queens to merge into one check
                continue
            if time_left() < self.time_left_threshold:
                return best_move_queen1, best_move_queen2, best_value, True
            forecast_board_with_move = game.forecast_move(move_queen1, move_queen2)
            _, _, value, _ = self.alphabeta(forecast_board_with_move, time_left, depth - 1, alpha, beta, False)
            # MAXIMUM over all moves
            if value > best_value:
                best_move_queen1 = move_queen1
                best_move_queen2 = move_queen2
                best_value = value
            if value >= beta:
                return best_move_queen1, best_move_queen2, best_value, False
            alpha = max(alpha, value)
            moves_values[('max', depth)].append((value, move_queen1, move_queen2))
        return best_move_queen1, best_move_queen2, best_value, False, moves_values
    else:
        best_move_queen1 = None
        best_move_queen2 = None
        best_value = float("inf")
        for move_queen1 in queen1_moves:
            for move_queen2 in queen2_moves:
                if move_queen1 == move_queen2:
                    continue
                if time_left() < self.time_left_threshold:
                    return best_move_queen1, best_move_queen2, best_value, True
                forecast_board_with_move = game.forecast_move(move_queen1, move_queen2)
                _, _, value, _ = self.alphabeta(forecast_board_with_move, time_left, depth - 1, alpha, beta, True)
                # MINIMUM over all moves
                if value < best_value:
                    best_move_queen1 = move_queen1
                    best_move_queen2 = move_queen2
                    best_value = value
                if value <= alpha:
                    return best_move_queen1, best_move_queen2, best_value, False
                beta = min(beta, value)
        return best_move_queen1, best_move_queen2, best_value, False

def alphabeta_with_iterative_deepening(self, game, time_left, initial_depth=2):
    current_depth = initial_depth
    best_move_queen1 = None
    best_move_queen2 = None
    best_value = None
    last_best_move_queen1 = None
    last_best_move_queen2 = None
    last_utility_value = None

    while time_left() > self.time_left_threshold:
        best_move_queen1, best_move_queen2, utility, ended_early = self.alphabeta(game, time_left, depth=current_depth)
        if ended_early:
            if (last_best_move_queen1 is not None) and (last_best_move_queen2 is not None) and (last_utility_value is not None):
                return last_best_move_queen1, last_best_move_queen2, last_utility_value
            # else:
            #     return best_move_queen1, best_move_queen2, best_value

        last_best_move_queen1 = best_move_queen1
        last_best_move_queen2 = best_move_queen2
        last_utility_value = utility
        current_depth += 1

    return last_best_move_queen1, last_best_move_queen2, last_utility_value
