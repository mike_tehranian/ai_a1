    def move(self, game, legal_moves, time_left):
        """Called to determine one move by your agent

        Note:
                1. Do NOT change the name of this 'move' function. We are going to call
                the this function directly.
                2. Change the name of minimax function to alphabeta function when
                required. Here we are talking about 'minimax' function call,
                NOT 'move' function name.

        Args:
            game (Board): The board and game state.
            legal_moves (dict): Dictionary of legal moves and their outcomes
            time_left (function): Used to determine time left before timeout

        Returns:
            (tuple, tuple): best_move_queen1, best_move_queen2
        """
        # Use legal_moves later with initial book moves (which are available)

        # best_move_queen1, best_move_queen2, utility = self.minimax(game, time_left, depth=self.search_depth)
        # best_move_queen1, best_move_queen2, utility = self.alphabeta(game, time_left, depth=self.search_depth)

        queen1_moves = queen1_moves = game.get_legal_moves_of_queen1()
        fake = game.__get_moves__(game.__last_queen_move__[game.get_queen_name(game.__active_players_queen1__)])
        fake2 = self.my_get_moves(game, game.__last_queen_move__[game.get_queen_name(game.__active_players_queen1__)])

        import ipdb; ipdb.set_trace()
        if queen1_moves == fake:
            print "good"
        if fake2 == fake:
            print "good2"
        else:
            print "nope"

        # exit()
        best_move_queen1, best_move_queen2, utility = self.alphabeta_with_iterative_deepening(game, time_left, initial_depth=self.search_depth)
        return best_move_queen1, best_move_queen2


    def my_get_moves(self, game, move):
        import ipdb; ipdb.set_trace()

        if move == game.NOT_MOVED:
            return game.get_first_moves()
        if game.move_count < 1:
            return game.get_first_moves()

        r, c = move

        directions = [ (-1, -1), (-1, 0), (-1, 1),
                        (0, -1),          (0,  1),
                        (1, -1), (1,  0), (1,  1)]

        fringe = [((r+dr,c+dc), (dr,dc)) for dr, dc in directions
                if game.move_is_legal(r+dr, c+dc)]

        import ipdb; ipdb.set_trace()
        valid_moves = []

        while fringe:
            move, delta = fringe.pop()

            r, c = move
            dr, dc = delta

            if game.move_is_legal(r,c):
                new_move = ((r+dr, c+dc), (dr,dc))
                fringe.append(new_move)
                valid_moves.append(move)

        import ipdb; ipdb.set_trace()
        return valid_moves



