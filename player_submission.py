#!/usr/bin/env python
from isolation import Board, game_as_text
from random import randint

# This file is your main submission that will be graded against. Do not
# add any classes or functions to this file that are not part of the classes
# that we want.

# TODO MDT double check I can import below
import itertools

class OpenMoveEvalFn:

    def score(self, game, maximizing_player_turn=True):
        """Score the current game state

        Evaluation function that outputs a score equal to how many
        moves are open for AI player on the board minus how many moves
        are open for Opponent's player on the board.

        Note:
                1. Be very careful while doing opponent's moves. You might end up
                   reducing your own moves.
                2. Here if you add overlapping moves of both queens, you are considering one available square twice.
                   Consider overlapping square only once. In both cases- myMoves and in OppMoves.
                3. If you think of better evaluation function, do it in CustomEvalFn below.

        Args
            param1 (Board): The board and game state.
            param2 (bool): True if maximizing player is active.

        Returns:
            float: The current state's score. MyMoves-OppMoves.

        """
        # (My legal moves - my duplicate moves) - (Opponent's legal moves - opponent's duplicate moves)

        active_player_moves = game.get_legal_moves()
        queen_moves = set()
        for queen, moves in active_player_moves.iteritems():
            queen_moves.update(moves)
        num_active_player_moves = len(queen_moves)

        inactive_player_moves = game.get_opponent_moves()
        queen_moves = set()
        for queen, moves in inactive_player_moves.iteritems():
            queen_moves.update(moves)
        num_inactive_player_moves = len(queen_moves)

        if maximizing_player_turn:
            # Try to maximize the overall score
            return num_active_player_moves - num_inactive_player_moves
        else:
            # Try to minimize the overall score
            return num_inactive_player_moves - num_active_player_moves


class CustomEvalFn:

    def __init__(self):
        pass

    def score(self, game, maximizing_player_turn=True):
        """Score the current game state

        Custom evaluation function that acts however you think it should. This
        is not required but highly encouraged if you want to build the best
        AI possible.

        Args
            game (Board): The board and game state.
            maximizing_player_turn (bool): True if maximizing player is active.

        Returns:
            float: The current state's score, based on your own heuristic.

        """
        # TODO - weight the moves differently based on location.
        # create a static matrix of values and their corresponding
        # location scores here and use that. Use can define the matrix in
        # the init method
        # TODO - create a linear weight of the move locations and queens
        # TODO - try multiplying the number of moves for players queen together.
        # so if player 1 q11 has 5 moves and q12 has 4 moves => 5*4 = 20.
        # Do similarly for player 2 and then take the difference
        # TODO - try to get as close to the opponents queens as possible to try to corner them
        # TODO - if # of opponents moves (inactive player??) is zero then boost the score, also
        # similarly try when my moves is zero then maybe lower the score further
        # TODO - be on the lookout for killer moves
        # TODO - get moves for q1 and q2 individually and look for the minimum value and weight on that
        # the goal is to find the weakest queen and go after that piece or to try to protect your weakest
        # piece as much as possible
        # TODO - look to surround and killer moves as much as possible
        # TODO - see if one of the opponents pieces has less moves than the other one and go after that piece
        # hunt down the weakest queen with the fewest moves!!! try to surround it as much as possible
        # if a piece is touching a wall, take both queens and surround the other sides and try to smother it

        my_ai_player_count = min(len(game.get_legal_moves_of_queen1()), len(game.get_legal_moves_of_queen2()))
        opp_ai_player_count = min(len(self.get_legal_moves_of_queen1_opp(game)), len(self.get_legal_moves_of_queen2_opp(game)))

        if my_ai_player_count == 0:
            if maximizing_player_turn:
                return -10000 + game.move_count
            else:
                return 10000 - game.move_count
        if opp_ai_player_count == 0:
            if maximizing_player_turn:
                return 10000 - game.move_count
            else:
                return -10000 + game.move_count

        active_player_moves = game.get_legal_moves()
        queen_moves = set()
        for queen, moves in active_player_moves.iteritems():
            queen_moves.update(moves)
        num_active_player_moves = len(queen_moves)

        inactive_player_moves = game.get_opponent_moves()
        queen_moves = set()
        for queen, moves in inactive_player_moves.iteritems():
            queen_moves.update(moves)
        num_inactive_player_moves = len(queen_moves)

        # More aggressive strategy to go after the opponent on the board
        # and try to isolate them aggressively.
        if maximizing_player_turn:
            # Try to maximize the overall score
            # return num_active_player_moves - (5.0 * num_inactive_player_moves)
            # return num_active_player_moves - (2.0 * opp_ai_player_count)
            return num_active_player_moves - (1 * num_inactive_player_moves) + my_ai_player_count - (1 * opp_ai_player_count)
        else:
            # Try to minimize the overall score
            # return num_inactive_player_moves - (5.0 * num_active_player_moves)
            # return opp_ai_player_count - (2.0 * num_active_player_moves)
            return (1 * num_inactive_player_moves) - num_active_player_moves + (1 * opp_ai_player_count) - my_ai_player_count

    def get_legal_moves_of_queen1_opp(self, game):
        return game.__get_moves__(game.__last_queen_move__[game.get_queen_name(game.__inactive_players_queen1__)])

    def get_legal_moves_of_queen2_opp(self, game):
        return game.__get_moves__(game.__last_queen_move__[game.get_queen_name(game.__inactive_players_queen2__)])

class CustomPlayer:
    """Player that chooses a move using
    your evaluation function and
    a minimax algorithm
    with alpha-beta pruning.
    You must finish and test this player
    to make sure it properly uses minimax
    and alpha-beta to return a good move."""

    def __init__(self, search_depth=2, eval_fn=CustomEvalFn()):
        """Initializes your player.

        if you find yourself with a superior eval function, update the default
        value of `eval_fn` to `CustomEvalFn()`

        Args:
            search_depth (int): The depth to which your agent will search
            eval_fn (function): Utility function used by your agent
        """
        self.eval_fn = eval_fn
        # TODO play with initial search depth
        self.search_depth = search_depth
        self.time_left_threshold = 90
        self.good_initial_moves = set([(3, 3), (1, 2), (2, 1), (4, 1), (5, 2), (1, 4), (2, 5), (4, 5), (5, 4), (1, 3), (5, 3)])

    def move(self, game, legal_moves, time_left):
        """Called to determine one move by your agent

        Note:
                1. Do NOT change the name of this 'move' function. We are going to call
                the this function directly.
                2. Change the name of minimax function to alphabeta function when
                required. Here we are talking about 'minimax' function call,
                NOT 'move' function name.

        Args:
            game (Board): The board and game state.
            legal_moves (dict): Dictionary of legal moves and their outcomes
            time_left (function): Used to determine time left before timeout

        Returns:
            (tuple, tuple): best_move_queen1, best_move_queen2
        """
        # best_move_queen1, best_move_queen2, utility = self.minimax(game, time_left, depth=self.search_depth)
        # best_move_queen1, best_move_queen2, utility = self.alphabeta(game, time_left, depth=self.search_depth)
        best_move_queen1, best_move_queen2, utility = self.alphabeta_with_iterative_deepening(game, time_left, initial_depth=self.search_depth)

        return best_move_queen1, best_move_queen2

    def utility(self, game, maximizing_player):
        """Can be updated if desired. Not compulsory. """
        # Consider using OpenMoveEvalFn when projecting opponents moves
        # that way the predicted behavior of the opponent is not distorted by my own custom evaluation function
        return self.eval_fn.score(game, maximizing_player)

    def minimax(self, game, time_left, depth, maximizing_player=True):
        """Implementation of the minimax algorithm

        Args:
            game (Board): A board and game state.
            time_left (function): Used to determine time left before timeout
            depth: Used to track how deep you are in the search tree
            maximizing_player (bool): True if maximizing player is active.

        Returns:
            (tuple,tuple, int): best_move_queen1,best_move_queen2, val
        """
        # Check to see if a terminal node
        # Code is staggered like this for performance gains
        if depth == 0:
            # Move and queen are known back in the prior function call, so use None placeholders
            return None, None, self.utility(game, maximizing_player)
        queen1_moves = None
        queen1_moves = game.get_legal_moves_of_queen1()
        if not queen1_moves:
            # Move and queen are known back in the prior function call, so use None placeholders
            return None, None, self.utility(game, maximizing_player)
        queen2_moves = None
        queen2_moves = game.get_legal_moves_of_queen2()
        if not queen2_moves:
            # Move and queen are known back in the prior function call, so use None placeholders
            return None, None, self.utility(game, maximizing_player)

        if maximizing_player:
            best_move_queen1 = None
            best_move_queen2 = None
            best_value = float("-inf")
            for move_queen1 in queen1_moves:
                for move_queen2 in queen2_moves:
                    if move_queen1 == move_queen2:
                        continue
                    if time_left() < self.time_left_threshold:
                        return best_move_queen1, best_move_queen2, best_value
                    forecast_board_with_move = game.forecast_move(move_queen1, move_queen2)
                    _, _, value = self.minimax(forecast_board_with_move, time_left, depth - 1, False)
                    # MAXIMUM over all moves
                    if value > best_value:
                        best_move_queen1 = move_queen1
                        best_move_queen2 = move_queen2
                        best_value = value
            return best_move_queen1, best_move_queen2, best_value
        else:
            best_move_queen1 = None
            best_move_queen2 = None
            best_value = float("inf")
            for move_queen1 in queen1_moves:
                for move_queen2 in queen2_moves:
                    if move_queen1 == move_queen2:
                        continue
                    if time_left() < self.time_left_threshold:
                        return best_move_queen1, best_move_queen2, best_value
                    forecast_board_with_move = game.forecast_move(move_queen1, move_queen2)
                    _, _, value = self.minimax(forecast_board_with_move, time_left, depth - 1, True)
                    # MINIMUM over all moves
                    if value < best_value:
                        best_move_queen1 = move_queen1
                        best_move_queen2 = move_queen2
                        best_value = value
            return best_move_queen1, best_move_queen2, best_value

    def alphabeta(self, game, time_left, depth, alpha=float("-inf"), beta=float("inf"), maximizing_player=True, previous_best_moves=None):
        """Implementation of the alphabeta algorithm

        Args:
            game (Board): A board and game state.
            time_left (function): Used to determine time left before timeout
            depth: Used to track how deep you are in the search tree
            alpha (float): Alpha value for pruning
            beta (float): Beta value for pruning
            maximizing_player (bool): True if maximizing player is active.

        Returns:
            (tuple,tuple, int): best_move_queen1,best_move_queen2, val
        """
        # Check to see if a terminal node
        if depth == 0:
            return None, None, self.utility(game, maximizing_player), False, None
        # Code is staggered like this for performance gains
        # Queen moves are known back in the prior function call so use None placeholders
        queen1_moves = game.get_legal_moves_of_queen1()
        if not queen1_moves:
            return None, None, self.utility(game, maximizing_player), False, None
        queen2_moves = game.get_legal_moves_of_queen2()
        if not queen2_moves:
            return None, None, self.utility(game, maximizing_player), False, None

        # TODO Forward pruning when # moves > 100. Ignore wall moves on first couple of moves

        if maximizing_player:
            best_move_queen1 = None
            best_move_queen2 = None
            best_value = float("-inf")
            # prior_move_calc = {}
            move_values = []

            ##### INITIAL BOOK MOVES
            # TODO play with the 1 number below and try increasing it and check bonnie performance
            if game.move_count <= 1:
                q1_ib_moves, q2_ib_moves = self.initial_book_moves(queen1_moves, queen2_moves, game)
                if q1_ib_moves and q2_ib_moves:
                    queen1_moves = q1_ib_moves
                    queen2_moves = q2_ib_moves
                elif q1_ib_moves and not q2_ib_moves:
                    queen1_moves = q1_ib_moves
                elif not q1_ib_moves and q2_ib_moves:
                    queen2_moves = q2_ib_moves
            #####

            if previous_best_moves is None:
                moves_to_check = itertools.product(queen1_moves, queen2_moves)
            else:
                moves_to_check = sorted(previous_best_moves, key=lambda x: x[2], reverse=True)
            for item in moves_to_check:
                move_queen1, move_queen2 = item[:2]
                if move_queen1 == move_queen2:
                    continue
                # if (move_queen2, move_queen1) in prior_move_calc:
                #     continue
                if time_left() < self.time_left_threshold:
                    return best_move_queen1, best_move_queen2, best_value, True, move_values
                forecast_board_with_move = game.forecast_move(move_queen1, move_queen2)
                _, _, value, _, _ = self.alphabeta(forecast_board_with_move, time_left, depth - 1, alpha, beta, False)
                # prior_move_calc[(move_queen1, move_queen2)] = value
                # MAXIMUM over all moves
                if value > best_value:
                    best_move_queen1 = move_queen1
                    best_move_queen2 = move_queen2
                    best_value = value
                if value >= beta:
                    return best_move_queen1, best_move_queen2, best_value, False, move_values
                alpha = max(alpha, value)
                move_values.append((move_queen1, move_queen2, value))
            return best_move_queen1, best_move_queen2, best_value, False, move_values
        else:
            best_move_queen1 = None
            best_move_queen2 = None
            best_value = float("inf")
            # prior_move_calc = {}
            for move_queen1 in queen1_moves:
                for move_queen2 in queen2_moves:
                    if move_queen1 == move_queen2:
                        continue
                    # if (move_queen2, move_queen1) in prior_move_calc:
                    #     continue
                    if time_left() < self.time_left_threshold:
                        return best_move_queen1, best_move_queen2, best_value, True, None
                    forecast_board_with_move = game.forecast_move(move_queen1, move_queen2)
                    _, _, value, _, _ = self.alphabeta(forecast_board_with_move, time_left, depth - 1, alpha, beta, True)
                    # prior_move_calc[(move_queen1, move_queen2)] = value
                    # MINIMUM over all moves
                    if value < best_value:
                        best_move_queen1 = move_queen1
                        best_move_queen2 = move_queen2
                        best_value = value
                    if value <= alpha:
                        return best_move_queen1, best_move_queen2, best_value, False, None
                    beta = min(beta, value)
            return best_move_queen1, best_move_queen2, best_value, False, None

    def alphabeta_with_iterative_deepening(self, game, time_left, initial_depth=2):
        current_depth = initial_depth
        best_move_queen1 = None
        best_move_queen2 = None
        best_value = None
        last_best_move_queen1 = None
        last_best_move_queen2 = None
        last_utility_value = None
        previous_best_moves = None
        quiescence_count = 0
        condition = True

        while condition:
            best_move_queen1, best_move_queen2, utility, ended_early, move_values = self.alphabeta(
                                                                        game,
                                                                        time_left,
                                                                        depth=current_depth,
                                                                        previous_best_moves=previous_best_moves)
            if move_values is None:
                # print "Lost Game. Move {}. Depth {}.".format(game.move_count, current_depth)
                return None, None, None
            # print "Move {}. Depth {}. Explored # moves: {}. Ended early: {}".format(game.move_count, current_depth, len(move_values), ended_early)

            if ended_early:
                if (best_move_queen1 is not None) and (best_move_queen2 is not None) and (utility is not None):
                    return best_move_queen1, best_move_queen2, utility
                elif (last_best_move_queen1 is not None) and (last_best_move_queen2 is not None) and (last_utility_value is not None):
                    return last_best_move_queen1, last_best_move_queen2, last_utility_value
                elif (last_best_move_queen1 is None) or (last_best_move_queen2 is None) or (last_utility_value is None):
                    print "Should never get here!"

            if last_best_move_queen1 == best_move_queen1 and last_best_move_queen2 == best_move_queen2 and last_utility_value == utility:
                quiescence_count += 1
                if quiescence_count > 3:
                    return last_best_move_queen1, last_best_move_queen2, last_utility_value
            else:
                quiescence_count = 0

            last_best_move_queen1 = best_move_queen1
            last_best_move_queen2 = best_move_queen2
            last_utility_value = utility
            previous_best_moves = move_values
            current_depth += 1
            condition = time_left() > self.time_left_threshold

        return last_best_move_queen1, last_best_move_queen2, last_utility_value

    def initial_book_moves(self, queen1_moves, queen2_moves, game):
        q1_moves = []
        q2_moves = []
        q1_ib = set((x,y) for x,y in queen1_moves)
        q2_ib = set((x,y) for x,y in queen2_moves)

        if game.move_count == 0:
            q1_moves.append((3, 3))
        # elif game.move_count == 1 and (3, 3) in q1_ib:
        #     q1_moves.append((3, 3))
        else:
            for ib_move in self.good_initial_moves:
                if ib_move in q1_ib:
                    q1_moves.append(ib_move)

        for ib_move in self.good_initial_moves:
            if ib_move in q2_ib:
                q2_moves.append(ib_move)

        return q1_moves, q2_moves
